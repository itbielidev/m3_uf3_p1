# M3: P01 - Gestió d'usuaris

## Autores
- Carolina Blanco Garrido
- Carlos Tarrias Diaz

[[_TOC_]]

## Objetivo

Realizar un programa que implementa un sistema de alta y gestión de usuarios para un gimnasio.
Así como gestionar copias de seguridad de los datos de los usuarios.

## Descripción
Implementación de un sistema de alta y gestión de usuarios, que permite registrar y modificar los datos de los 
clientes así como realizar copias de seguridad diarias de los datos. 
- El sistema tiene 3 directorios que nos permiten el manejo de ficheros. Estos directorios son:
1. `import`: Este directorio contiene los ficheros que hay que importar.
2. `backups`: Este directorio contiene los ficheros del backup.
3. `data`: Este directorio contiene el fichero `userData.txt`

## Funcionalidades

- Importación de datos: Comprueba si existen ficheros con usuarios del viejo sistema dentro de la carpeta `import`. Si es así, añade los datos (sin modificarlos) de los distintos usuarios (en el formato específico acordado) en un nuevo fichero de texto dentro de la carpeta `data`.
Después del proceso se eliminan los ficheros de importación.
- Funcionalidad del sistema: Las siguientes funcionalidades presentes en el menú nos permiten: 
  - Creación del usuario: Mediante la función de `buildUser()`, creamos los usuarios. Esta función construye un usuario que lo guarda en un array de strings.
  
  - Modificación del usuario: La función `modifyUser()`permite modificar usuarios en el programa. Una vez modificado se actualiza el fichero `userData.txt`.
  - Des/bloqueo del usuario: Permite modificar el campo `blocked` de un usuario concreto. Si el campo es `false` pasa a `true`, y viceversa.
  - Salir: Permite salir del menú principal del sistema.
- Gestor de copias de seguridad (backups):
  - Sólo se permite hacer un backup al dia.
  - La función `backup()` hace un backup de los datos de lo usuarios
  - Crea un fichero en el directorio `backups`. 
  - Como sólo pude hacer uno por dia, en el caso que ya exista uno, este se sobreescribe. Por otra parte, si no existe se crea.

