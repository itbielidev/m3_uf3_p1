/**
 * @author: Carolina Blanco y Carlos Tarrias
 * Módulo: M03 Programació
 * @description: Programa que implementa un sistema de alta y gestión de usuarios para un gimnasio.
 * Así como gestionar copias de seguridad de los datos de los usuarios.*/

fun main() {

  //Llamamos al proceso de importación de datos cada vez que se inicia el programa.
  importData("./import/", "./data/userData.txt")
  var userOption: Int

  //Menú principal de la aplicación.
  do {
    printMenu()
    userOption = readln().toInt()
    manageOption(userOption)
  }while(userOption != 0)
}
