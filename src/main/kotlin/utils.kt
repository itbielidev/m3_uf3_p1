import java.io.File
import java.nio.file.Path
import java.time.LocalDate
import kotlin.io.path.*

/**
 * Imprime el menú principal del programa por consola.
 */
fun printMenu() {
  val menu= """
  |1.Importar datos
  |2.Añadir usuario
  |3.Modificar usuario
  |4.Desbloquar/Bloquear usuario
  |5.Crear backup.
  |0.Salir del programa.
  """.trimMargin()

  println(menu)
  println("Escoge una opción:")
  print(">")
}

/**
 * Llama a la función concreta del sistema que haya decidido el usuario.
 * @param option - Int : Almacena el número (opción de menú) escogido por el usuario.
 */
fun manageOption(option:Int) {
  val id : Int
  when(option) {
    1-> importData("./import/", "./data/userData.txt")  //Realizamos la importación de datos.
    2-> addUser() //Insertamos un nuevo usuario en el sistema.
    3-> { //Actualizamos un usuario del sistema.
      id = promptId()
      modifyUser(id)
    }
    4-> { //Modificamos estado del bloqueo del usuario.
      id = promptId()
      changeUserState(id, "./data/userData.txt", "blocked")
    }
    5 -> backup() //Realizamos copia de seguridad
    0 -> { //Al salir, si no se detecta una copia de seguridad se realiza una.
      println("Saliendo del programa...")
      backup(true)
    }
    else -> println("La opción escogida no existe. Vuélvelo a intentar.")
  }
}

/**
 * Permite añadir usuarios en el programa.Si se ha añadido correctamente, se pinta un mensaje con el id que se le ha asignado al usuario.
 */
fun addUser() {
  val targetFilePath = "./data/userData.txt"
  val id = getLastIdRegistered(targetFilePath)
  val userFields = buildUser()

  writeToSystem(userFields, File(targetFilePath), id)

  println("Usuario añadido con id: $id")
}

/**
 * Recibe como @param el id del cliente, permite modificar usuarios en el programa. Una vez modificado se actualiza el fichero userData.txt.
 */
fun modifyUser(id: Int) {
  val userFields = buildUser()

  changeUserState(id, "./data/userData.txt", "update", userFields)
}
/**
 * Construye un usuario que lo guarda en un array de strings. Y posteriormente hace un @return del array.
 */
fun buildUser(): Array<String> {
  println("Introduce el nombre del usuario:")
  val name = readln()
  println("Introduce el teléfono del usuario:")
  val phone = readln()
  println("Introduce el email del usuario:")
  val email = readln()

  return arrayOf(name, phone, email,"true", "false")
}

/**
 * Pide al usuario un número de id y realiza el control de este número.
 * @return Int : Representa el id introducido por el usuario.
 */
fun promptId(): Int {
  println("Escoge la id:")
  var id = readln().toInt()
  while (id <= 0) {
    println("La id introducida no es correcta (tiene que ser mayor que 0). Vuelvélo a intentar")
    id = readln().toInt()
  }

  return id
}

//
/**
 * Función que comprueba si existe un usuario en el sistema con un id concreto.
 * @param id - Int : Representa el id numérico del usuario.
 * @param users - List<String> : Contiene todos los usuarios del sistema, cada uno representado como un String.
 * @return Int : Índice que indica la posición del usuario con el id concreto dentro de la lista de usuarios. Es -1 si no existe este usuario en la lista.
 */
fun idExists(id:Int, users:List<String>):Int {
  users.forEachIndexed { index, user ->
    val userId = user.split(";")[0].toInt()
    if (userId == id) return index
  }
  return -1
}

//
/**
 * Función que a partir de un id, modifica el campo "blocked" (o actualiza los campos del usuario) con ese id.
 * @param id - Int : Representa el id numérico del usuario.
 * @param dataPath - String : Representa la ruta hacia el fichero con los usuarios del nuevo sistema.
 * @param option - String : Indica si se usa la función para cambiar el campo "blocked", o, por el contrario, si se usa para cambiar los campos (nombre,emial,phone) del usuario.
 * @param updatedParams - Array<String> : En caso de que se vaya a actualizar los campos (nombre, email, phone), contiene los nuevos valores con los que se va a actualizar el usuario.
 */
fun changeUserState(id:Int, dataPath:String, option:String, updatedParams: Array<String> = arrayOf()) {
  val dataFile = File(dataPath)
  if (dataFile.exists() && dataFile.length()>0) {
    val users = dataFile.readLines() //Obtenemos lista con todos los usuarios del sistema.
    val indexOfTargetUser = idExists(id, users) //Comprobamos si el usuario existe y recuperamos su índice en la lista.
    if (indexOfTargetUser >= 0) { //Si hemos encontrado al usuario se sobreescribe el fichero con los datos actualizados.
      println("Usuario con id $id encontrado. Modificando...")
      dataFile.delete()
      dataFile.createNewFile()
      users.forEachIndexed { index, user ->
        if (index != indexOfTargetUser) dataFile.appendText(user+"\n") //No modificamos el resto de usuarios.
        else { //Invertimos el valor de blocked del usuario con el id específico.
          val blocked: String
          val targetUser = users[indexOfTargetUser].split(";")
          //Modificamos el usuario (nombre, email,phone)
          if (option == "update") {
            dataFile.appendText("${targetUser[0]};${updatedParams[0]};${updatedParams[1]};${updatedParams[2]};${targetUser[4]};${targetUser[5]}\n")
          }
          else if (option == "blocked"){ //o cambiamos su estado de bloqueado.
            blocked = if (targetUser.last() == "true") "false" else "true"
            dataFile.appendText("${targetUser[0]};${targetUser[1]};${targetUser[2]};${targetUser[3]};${targetUser[4]};$blocked\n")
          }

        }
      }
    }
    else { //Si no lo hemos encontrado mostramos mensaje de error.
      println("Error -> No existe ningún usuario con esta id.")
    }
  }
}

/**
 * Realiza un backup de los datos de lo usuarios. Recibe por @param exit con valor por defecto false. Crea un
 * fichero en el directorio backups. Como sólo pude hacer uno por dia, en el caso que ya exista uno, este se
 * sobreescribe, si el @param exit es false. Es decir si este método, no se llama desde el menú. Por otra parte
 * si no existe se crea.
 */
fun backup(exit: Boolean = false) {
  val currentDate = LocalDate.now()
  val backupPrefix = "${currentDate}userData.txt"
  val dataFile = File("./data/userData.txt")
  val backupFile = File("./backups/$backupPrefix")

  if (backupFile.exists()) {
    if (exit) return

    backupFile.delete()
  }

  backupFile.createNewFile()

  backupFile.appendText(dataFile.readText())

}

/**
 * Calcula el id a partir del cual debemos empezar a importar usuarios.
 * @param dataPath - String : Representa la ruta hacia el fichero con los usuarios del nuevo sistema.
 * @return Int : Representa el siguiente id que se añadirá al sistema.
 */
fun getLastIdRegistered(dataPath:String): Int {
  val f1 = File(dataPath)
  return if (f1.exists() && f1.length()>0) { //Ya tenemos usuarios registrados en el nuevo sistema.
    val lines = f1.readLines()
    //Nos interesa empezar a registrar la id después del id del último usuario registrado en importaciones anteriores.
    lines.last().split(";")[0].toInt() + 1 //El campo id es el primer campo de un usuario
  }
  else 1 //No tenemos usuarios registrados en el nuevo sistema, empezamos a registar con el id=1.
}

/**
 * Actualiza el fichero de datos del nuevo sistema con los campos del usuario.
 * @param userFields - Array<String> : Contiene los campos de usuario (excepto el id) que se introducirán al sistema.
 * @param targetFile - File : Referencia al fichero de datos del nuevo sistema.
 * @param countId - Int : Id que se añadirá para el nuevo usuario.
 * @return Int - Indica el id que tendrá el usuario de la siguiente inserción.
 */
fun writeToSystem(userFields: Array<String>, targetFile:File, countId:Int):Int {
  var id = countId
  if (!targetFile.exists()) targetFile.createNewFile()
  targetFile.appendText( //Hay que tener en cuenta que el fichero de sistema puede tener datos insertados.
    "$countId;${userFields[0]};${userFields[1]};${userFields[2]};${userFields[3]};${userFields[4]}\n")
  id++ //Incrementamos el id para la siguiente inserción de usuario en el sistema.
  return id
}

/**
 * Se encarga de leer/extraer los usuarios de un fichero de importación.
 * @param fileName - String : Ruta hacia el archivo de importación concreto.
 * @param targetFile - File : Referencia al fichero de datos del nuevo sistema.
 */
fun readFiles(fileName:String, targetFile:File) {
  val dataFile = File(fileName)
  val users: List<String>
  var userFields : List<String>

  //Campos de usuario
  //Como el id debe ser un valor consecutivo primero comprobamos si ya hay usuarios existentes en el sistema.
  var id:Int= getLastIdRegistered("./data/userData.txt")

  var name: String
  var phoneNumber: String
  var emailAddress: String
  var active: String
  var blocked: String

  //Si hay datos que leer en los ficheros.
  if (dataFile.length() > 0) {
    users = dataFile.readText().split(";") //Separamos los datos por usuarios, cada usuario acaba en ;
    for (user in users) {
      if (user != "") {
        userFields = user.split(",") //Separamos los campos de los usuarios, cada campo está separado por ,
        //En el fichero de importación los datos de los usuarios pueden estar en diferentes líneas
        //con tal que en el nuevo sistema cada usuario esté en una línea, debemos eliminar los carácteres especiales que
        //tengan los campos.
        name = userFields[1].replace("[\r\n\t]".toRegex(), " ").trim()
        phoneNumber = userFields[2].replace("[\r\n\t]".toRegex(), " ").trim()
        emailAddress = userFields[3].replace("[\r\n\t]".toRegex(), " ").trim()
        active = userFields[4].replace("[\r\n\t]".toRegex(), " ").trim()
        blocked = userFields[5].replace("[\r\n\t]".toRegex(), " ").trim()
        if (isValidUser(active.toBoolean(),blocked.toBoolean())) {
          //Insertamos la línea de usuario en el ficheros userData.txt en ./data (también tenemos que ir incrementando el id).
          id = writeToSystem(arrayOf(name,phoneNumber,emailAddress,active,blocked), targetFile, id)
        }
      }
    }
  }
}

/**
 * Comprueba si el usuario tiene los parámetros adecuados para ser importado al nuevo sistema.
 * @param active - Boolean : Almacena el campo "active"" del usuario.
 * @param blocked - Boolean : Almacena el campo "blocked" del usuario.
 * @return - Boolean : Indica si el usuario cumple la propiedad o no.
 */
fun isValidUser(active:Boolean, blocked:Boolean): Boolean = active && !blocked

/**
 * Importa los datos de los ficheros dentro de ./import/ al nuevo sistema.
 * @param importPath - String : Indica la ruta hacia la carpeta que contiene los ficheros de importación.
 * @param targetFile - String : Indica la ruta hacia el fichero donde se almacenarán los datos del nuevo sistema.S
 */
fun importData(importPath: String, targetFile:String) {
  val importDirectory = Path(importPath)
  val target = File(targetFile)
  println("Comprobando si existen ficheros de importación...")
  if (hasFiles(importDirectory)) {
    println("Se han detectado ficheros de usuarios. Realizando importación de datos...")
    for (file in importDirectory.listDirectoryEntries()) {
      //println(importPath + file.fileName.toString())
      readFiles(importPath + file.fileName.toString(), target)
      //Eliminamos los ficheros de importación.
      file.deleteIfExists()
    }
  }
  else {
    println("No existen ficheros de importación. Cancelando operación...")
  }
}

/**
 * Indica si hay ficheros dentro de una carpeta del sistema de archivos.
 * @param directory - Path : Referencia a la ruta del sistema de archivos.
 * @return - Boolean : Indica si la ruta contiene archivos o no.
 */
fun hasFiles(directory:Path) : Boolean {
  if (isDirectoryAndExists(directory)) return directory.listDirectoryEntries().isNotEmpty()
  return false
}

/**
 * Comprueba si la ruta existe dentro del sistema de archivos del proyecto y si corresponde a un directorio.
 * @param directory - Path : Referencia a la ruta del sistema de archivos.
 * @return - Boolean : Indica si la ruta es un directorio existente del sistema.
 */
fun isDirectoryAndExists(directory: Path): Boolean = directory.exists() && directory.isDirectory()


